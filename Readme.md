# Voxiva presentación

La presentación consiste en explicar el proceso completo que se lleva a cabo para analizar datos de cultivos de palta hass utilizando información satelital.

## Etapas

1. Descarga de imágenes satelitales.
2. Cálculo de NDVI (Índice de vegetación de diferencia normalizada).
3. Visualización de información.
4. *Clusterización.
5. *Estimación.
6. Conclusiones.

## Recursos

- _available_dates.csv:_ Archivo .csv separado por comas (','). Contiene las fechas de las imágenes satelitales limpias y disponibles para descargar.

- _farm.json:_ Contiene las coordenadas del cultivo. Útil para plotear el cultivo por lotes y para cropear las imágenes satelitales al área necesaria.
